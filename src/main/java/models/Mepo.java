package models;

import java.util.Date;

/**
 * User: cgatay
 * Date: 08/12/12
 * Time: 17:37
 */
public class Mepo {
    private String author;
    private String content;
    private Date createdAt;

    public Mepo() {
        this.createdAt = new Date();
    }

    public Mepo(final String author, final String content) {
        this(author, content, new Date());
    }

    public Mepo(final String author, final String content, final Date createdAt) {
        this.author = author;
        this.content = content;
        this.createdAt = createdAt;
    }

    /************************** GETTER / SETTERS ****************************/
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(final Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(final String author) {
        this.author = author;
    }
}
