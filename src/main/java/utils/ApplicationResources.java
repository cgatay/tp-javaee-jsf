package utils;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;

/**
 * User: cgatay
 * Date: 09/12/12
 * Time: 12:57
 */
public class ApplicationResources {
    /**
     * Query String to allow clean redirect by JSF.
     */
    public static final String FACES_REDIRECT = "?faces-redirect=true";

    /**
     * @return the current faces context
     */
    @Produces
    @RequestScoped
    public FacesContext getFacesContext(){
        return FacesContext.getCurrentInstance();
    }
}
