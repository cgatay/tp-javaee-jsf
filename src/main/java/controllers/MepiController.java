package controllers;

import models.Mepo;
import models.UserInfo;
import services.MepiService;
import utils.ApplicationResources;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * User: cgatay
 * Date: 09/12/12
 * Time: 18:36
 */
@Named
@RequestScoped
public class MepiController {
    @Inject
    UserInfo userInfo;
    @Inject
    MepiService mepiService;

    private Mepo newMepo;

    public String insertMepo(){
        //here we persist our Mepo value
        mepiService.insertMepo(newMepo);
        return "mepi"+ ApplicationResources.FACES_REDIRECT;
    }

    @Produces
    @Named
    public Mepo getNewMepo(){
        return newMepo;
    }

    @PostConstruct
    public void initClass(){
        newMepo = new Mepo();
        if (userInfo.getLoggedIn()){
            newMepo.setAuthor(userInfo.getNickName());
        }
    }
}
