package services;

import models.Mepo;
import models.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

/**
 * This class is annotated with the Stateless marker. It allows to automatically handle transactions.
 *
 * User: cgatay
 * Date: 08/12/12
 * Time: 20:19
 */
@Stateless
public class MepiService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MepiService.class);
    @Inject
    Event<Mepo> mepoEvent;
    @Inject
    UserInfo userInfo;


    public void insertMepo(final Mepo newMepo){
        LOGGER.info("We are inserting a new Mepo");
        mepoEvent.fire(newMepo);
    }

}
