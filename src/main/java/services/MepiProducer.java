package services;

import models.Mepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * User: cgatay
 * Date: 08/12/12
 * Time: 17:56
 */
@ApplicationScoped
public class MepiProducer {
    private static final Logger LOGGER = LoggerFactory.getLogger(MepiProducer.class);
    private List<Mepo> mepi;

    @PostConstruct
    public void initList() {
        mepi = new ArrayList<Mepo>();
        mepi.add(new Mepo("John Smice", "This is my first Mepo, awesome isn't it ?"));
        mepi.add(new Mepo("Poca Hontas", "Where are you John ?"));
        LOGGER.info("Mepi repository initialized");
    }

    /**
     * Observer method called whenever a mepo is inserted.
     * @param newMepo mepo to insert
     */
    public void onMepiInserted(@Observes(notifyObserver = Reception.IF_EXISTS) Mepo newMepo){
        LOGGER.info("Caught event, adding newly inserted Mepo to list.");
        mepi.add(0, newMepo);
    }

    /**
     * This method returns the list of the available mepi.
     * It is annotated with the @Produces annotation to tell that it generates values
     * It is annotated with the @Named annotation to be able to call it by name in mepi.jsf
     * @return list of mepi.
     */
    @Produces
    @Named
    public List<Mepo> loadMepi() {
        return mepi;
    }

}
